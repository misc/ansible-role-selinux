Ansible role to enforce SELinux policy

## Usage

To read parameters documentation, use this command:

```
ansible-doc -t role selinux
```

With this role the policy is always `enforcing`, only the policy type can be set.
